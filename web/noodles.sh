#!/bin/sh
echo "🚀 starting 🍜 server..."
echo "⏳ 🐳 restart stopped 🍜 containers"
podman restart --all
echo "🍜 template 🐳 images:"
podman images --filter=reference='noodles/*'
echo "🍜 function 🐳 images:"
podman images --filter=reference='noodles-*'
echo "🍜 🐳 (running) containers:"
podman ps -a --filter name="noodles-"
echo "🚀 🍜 Noodles ignition"
npm start

