const version="0.0.2"
const express = require('express')
const cp = require('child_process')
const yaml = require('js-yaml')
const fs = require('fs')

//const path = require('path')
const multer  = require('multer')

const app = express()
const port = process.env.PORT || 9090
const adminToken = process.env.ADMIN_TOKEN || "BOBMORANE"
const functionsPath = process.env.FUNCTIONS_PATH || `./functions`

app.use(express.static('public'))
app.use(express.json());

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, functionsPath)
  },
  filename: (req, file, cb) => {
    fs.mkdir(`${functionsPath}/${req.body.function}-${req.body.branch}`, 
      { recursive: true }, 
      (err) => { 
        if (err) {
          // TODO: directory already exists
        }
        cb(null, `/${req.body.function}-${req.body.branch}/${file.originalname}`)
      }
    )
  }
})

const upload = multer({ storage: storage })


let executeFunction = (req, res) => {
  let getParams = (req) => {
    if(req.body.params==null) {
      if(req.params["parameters"]) {
        return req.params["parameters"]
      } else {
        return ""
      }
    } else {
      return req.body.params
    }
  }
  try {
    let name = req.params["name"]
    let branch = req.params["branch"] 
    //let params = req.body.params==null ? "" : req.body.params
    let params = getParams(req)

    let path = `${functionsPath}/${name}-${branch}`
    let content = yaml.safeLoad(fs.readFileSync(`${path}/config.yml`, 'utf8'))

    let options = {
      timeout: 2000,
      killSignal: 'SIGKILL'
    }
    // podman run --name=noodles-hello-node-html-2-master-container -p 8080:8080
    //docker exec hi-master-container node ./index.js {name:"bob morane"}
    cp.exec(`cd ${path}; podman exec noodles-${name}-${branch}-container ${content.command} ${params}`, options, (error, stdout, stderr) => {

      if(error) {
        res.status(500)
        res.json({error: error, message:"😡 when calling function error"})  
      } else {
        // application/javascript
        // text/javascript
        switch(content["content-type"]) {
          case "json":
            res.json({result: stdout})
            break
          case "html":
            res.set('Content-Type', 'text/html')
            res.send(stdout)
            break
          case "text":
            res.set('Content-Type', 'text/plain')
            res.send(stdout)
            break
          case "javascript":
            //res.set('Content-Type', 'application/javascript')
            res.contentType('js')
            res.send(stdout)
            break
          default:
            res.json({result: stdout})
        }
      }
    })
  } catch (error) {
    res.status(500)
    res.json({error: error, message:"🥶 before calling function error"})  
  }
}

app.post('/function/:name/:branch', (req, res) => {
  executeFunction(req, res)
})

app.get('/function/:name/:branch', (req, res) => {
  executeFunction(req, res)
})

app.get('/function/:name/:branch/:parameters', (req, res) => {
  executeFunction(req, res)
})

let deleteFunction = (req, res) => {
  if(req.get("ADMIN_TOKEN")==adminToken) {
    try {
      let functionName = req.params["name"]
      let branch = req.params["branch"] 
  
      let path = `${functionsPath}/${functionName}-${branch}`
      let stop = `podman stop noodles-${functionName}-${branch}-container`
      let remove = `podman rm noodles-${functionName}-${branch}-container`

      cp.exec(`cd ${path}; ${stop}; ${remove};`, (error, stdout, stderr) => {
        console.log(stdout)
        if(error) {
          console.log(error)
          console.log(stderr)
          res.status(500)
          res.json({error: error, message:"😡 execution error when deleting container"})
        } else {

          cp.exec(`rm -rf ${path}`, (error, stdout, stderr) => {
            if(error) {
              res.status(500)
              res.json({error: error, message:"😡 execution error when remove files"})
            } else {
              res.json({functionName, branch, authenticated:true, deleted:true})
              //res.json({build: "OK", message:`${req.body.function}-${req.body.branch} built`})
            }
          })    
        }
      })

      
    } catch (error) {
      res.status(500)
      res.json({error: error, message:"🥶 before delete error"})  
    }
  } else {
    res.status(500)
    res.json({authenticated:false, message:"🤬 bad token"})
  }

}

app.delete('/function/:name/:branch', (req, res) => {
  deleteFunction(req, res)
})

app.post('/publish', upload.array('file'), (req,res) => {

  if(req.get("ADMIN_TOKEN")==adminToken) {
    req.files.filter(item => item.originalname.endsWith(".sh"))
      .forEach(file => {
        console.log("😃", file)
        fs.chmod(
          `${functionsPath}/${req.body.function}-${req.body.branch}/${file.originalname}`, 
          0755, 
          ()=>{}
        )      
      })

      console.log("uploaded 🚀")
      console.log("now building 🐳")

      /* shell sample
        # == build image and start the function container
        cd demos/${FUNCTION_NAME}
        # === build
        podman build -t ${IMAGE_NAME} .
        # === create and start the container
        # > warning 🖐 it works only with alpine flavored images
        # stop and remove the container (if exists)
        if [ "$(podman ps -q -f name=${CONTAINER_NAME})" ]; then
          podman stop ${CONTAINER_NAME}
          podman rm ${CONTAINER_NAME}
        fi      
        # start the container
        podman run -it -d --name ${CONTAINER_NAME} ${IMAGE_NAME}:latest sh  
      */

      let path = `${functionsPath}/${req.body.function}-${req.body.branch}`
      let build = `podman build -t noodles-${req.body.function}-${req.body.branch}-img .`
      let stop = `podman stop noodles-${req.body.function}-${req.body.branch}-container`
      let remove = `podman rm noodles-${req.body.function}-${req.body.branch}-container`
      //let testIfRunning = `if [ "$(podman ps -q -f name=noodles-${req.body.function}-${req.body.branch}-container" ]; ${stop}; ${remove}; fi`
      // warning 🖐 it works only with alpine flavored images

      // podman stop noodles-hello-node-html-2-master-container
      // podman rm noodles-hello-node-html-2-master-container
      // podman run -p 8080:8080 -d --name=noodles-hello-node-html-2-master-container noodles-hello-node-html-2-master-img:latest 
      // podman restart noodles-hello-node-html-2-master-container

      // how to manage the ports / proxy / reverse ...

      let start = `podman run -it -d --name noodles-${req.body.function}-${req.body.branch}-container noodles-${req.body.function}-${req.body.branch}-img:latest sh`
      try {
        cp.exec(`cd ${path}; ${build}; ${stop}; ${remove}; ${start};`, (error, stdout, stderr) => {
          console.log(stdout)
          if(error) {
            console.log(error)
            console.log(stderr)
            res.status(500)
            res.json({error: error, message:"😠 build error"})
          } else {
            res.json({build: true, message:`🙂 ${req.body.function}-${req.body.branch} built`})
          }
        })

      } catch(error) {
        res.status(500)
        res.json({error: error, message:"🥶 before build error"})
      }

  } else {
    res.status(500)
    res.json({authenticated:false, message:"🤬 bad token"})
  }

})

// running(?) containers
app.get('/functions/containers', (req, res) => {
  try {
    cp.exec(`podman ps -a --filter name="noodles-" --format '{{ .Names}}'`, (error, stdout, stderr) => {
      let results = stdout.toString()
      //console.log(results)
      if(error) {
        res.status(500)
        res.json({error: error, message:"😠 error"})
      } else {
        res.json({containers: results.split("\n")})
      }
    })

  } catch(error) {
    res.status(500)
    res.json({error: error, message:"🥶 before error"})
  }
})

app.get('/functions/images', (req, res) => {
  try {
    cp.exec(`podman images --filter=reference='noodles-*' --format '{{ .Repository}}'`, (error, stdout, stderr) => {
      let results = stdout.toString()
      //console.log(results)
      if(error) {
        res.status(500)
        res.json({error: error, message:"😠 error"})
      } else {
        res.json({images: results.split("\n")})
      }
    })

  } catch(error) {
    res.status(500)
    res.json({error: error, message:"🥶 before error"})
  }
})

app.get('/about', (req, res) => {
  res.set('Content-Type', 'text/plain')
  res.send(`noodles-faas v${version} ☢️ don't use it in production`)
})

app.listen(port, () => console.log(
`
   / | / /___  ____  ____/ / /__  _____
  /  |/ / __ \/ __ \/ __  / / _ \/ ___/
 / /|  / /_/ / /_/ / /_/ / /  __(__  ) 
/_/ |_/\____/\____/\__,_/_/\___/____/ 
🌍 🐳 🍜 Noodles faas v${version} is listening on port ${port}!
`
))