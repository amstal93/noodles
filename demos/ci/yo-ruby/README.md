## How to use this demo

```shell
git init
git add .
git commit -m "🎉 first commit"
# publish the function
gitlab-runner exec shell deploy_function
# run(call) the function
gitlab-runner exec shell run_function
```